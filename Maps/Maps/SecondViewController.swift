//
//  SecondViewController.swift
//  Maps
//
//  Created by Teiksma  on 31.03.18.
//  Copyright © 2018. g. janisengers. All rights reserved.
//

import UIKit
import MapKit

class SecondViewController: UIViewController,UITextFieldDelegate
{
    var delegate: SecondViewControllerDelegate?

    @IBOutlet weak var mapPointsFilterSegmentControl: UISegmentedControl!
    @IBOutlet weak var textboxNoticeLabel: UILabel!
    
    @IBOutlet weak var latitudeTexbox: UITextField!
    @IBOutlet weak var longitudeTextbox: UITextField!
    let defaults = UserDefaults.standard
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        mapPointsFilterSegmentControl.selectedSegmentIndex = defaults.integer(forKey: "mapPointsFilterIndex")
        latitudeTexbox.text = defaults.string(forKey: "latitude")
        longitudeTextbox.text = defaults.string(forKey: "longitude")
        
        self.latitudeTexbox.delegate     = self
        self.longitudeTextbox.delegate   = self
        
        if mapPointsFilterSegmentControl.selectedSegmentIndex == 1
        {
            _ = self.validateTextfields()
        }
    }
    
    @IBAction func toggleMapPointsToShow(_ sender: UISegmentedControl)
    {
        let index = sender.selectedSegmentIndex
        defaults.set(index, forKey: "mapPointsFilterIndex")
        
        switch index
        {
            case 0:
                delegate?.showAllMapPoints()
                self.textboxNoticeLabel.isHidden = true
            case 1:
                self.showOnlyNearbyMapPoints()
            case 2:
                 delegate?.showOnlyWithDescriptionMapPoints()
                 self.textboxNoticeLabel.isHidden = true
            default: break
        }
    }
    
    @IBAction func coordinatesTextFieldUpdate()
    {
        defaults.set(latitudeTexbox.text!, forKey: "latitude")
        defaults.set(longitudeTextbox.text!, forKey: "longitude")
        
        if mapPointsFilterSegmentControl.selectedSegmentIndex == 1
        {
            self.showOnlyNearbyMapPoints()
        }
    }
    
    func showOnlyNearbyMapPoints()
    {
        if self.validateTextfields()
        {
            delegate?.showOnlyNearbyMapPoints(fromLatitude: Double(latitudeTexbox.text!)!,fromLongitude:Double(longitudeTextbox.text!)!)
        }
    }
    
    func validateTextfields()->Bool
    {
        if  let latidude     = Double(latitudeTexbox.text!),
            let longitude    = Double(longitudeTextbox.text!)
        {
            let loc = CLLocationCoordinate2D(latitude: latidude,longitude:longitude)
            
            if CLLocationCoordinate2DIsValid(loc)
            {
                self.textboxNoticeLabel.isHidden = true
                
                return true
            }
            else
            {
                self.textboxNoticeLabel.textColor = #colorLiteral(red: 0.949, green: 0.1098, blue: 0, alpha: 1)
                self.textboxNoticeLabel.text = "Invalid coordinates"
                self.textboxNoticeLabel.isHidden = false
                
                return false
            }
        }
        else
        {
            self.textboxNoticeLabel.textColor = #colorLiteral(red: 0.1947486302, green: 0.4189995233, blue: 1, alpha: 1)
            self.textboxNoticeLabel.text = "Enter latitude and longitude"
            self.textboxNoticeLabel.isHidden = false
            
            return false
        }
    }
    
    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool
    {
        if textField.text != "" || string != ""
        {
            let res = (textField.text ?? "") + string
            return Double(res) != nil
        }
        
        return true
    }
    
}

protocol SecondViewControllerDelegate: class
{
    func showOnlyNearbyMapPoints(fromLatitude:Double, fromLongitude:Double)
    func showAllMapPoints()
    func showOnlyWithDescriptionMapPoints()
}
