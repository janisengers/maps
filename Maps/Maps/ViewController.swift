//
//  ViewController.swift
//  Maps
//
//  Created by Teiksma  on 31.03.18.
//  Copyright © 2018. g. janisengers. All rights reserved.
//

import UIKit
import MapKit

class ViewController: UIViewController, ThirdViewControllerDelegate, SecondViewControllerDelegate, CLLocationManagerDelegate, MKMapViewDelegate
{
    let defaults = UserDefaults.standard

    var myLocation: CLLocationCoordinate2D = CLLocationCoordinate2D()
    
    let locationManager = CLLocationManager()

    @IBOutlet weak var mapView: MKMapView!
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        locationManager.delegate = self
        locationManager.requestWhenInUseAuthorization()
        locationManager.startUpdatingLocation()
        
        self.myLocation = mapView.centerCoordinate
        
        switch defaults.integer(forKey: "mapPointsFilterIndex")
        {
            case 0:
                showAllMapPoints()
            case 1:
                if defaults.string(forKey: "latitude") != "", defaults.string(forKey: "longitude") != ""
                {
                    showOnlyNearbyMapPoints(fromLatitude: Double(defaults.string(forKey: "latitude")!)!, fromLongitude: Double(defaults.string(forKey: "longitude")!)!)
                }
                else
                {
                   showAllMapPoints()
                }
            case 2:
                showOnlyWithDescriptionMapPoints()
            default: break
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?)
    {
        if let filtersView = segue.destination as? SecondViewController
        {
            filtersView.delegate = self
        }
        
        if let addNewView = segue.destination as? ThirdViewController
        {
            addNewView.delegate = self
        }
    }
    
    func addMapPoints(points: Array<NSDictionary>)
    {
        for point in points
        {
            let title       = point["Title"] as! String
            let latitude    = point["Latitude"] as! String
            let longitude   = point["Longitude"] as! String
            let description   = point["Description"] as! String
            
            if !(title.isEmpty),!(latitude.isEmpty),!(longitude.isEmpty)
            {
                self.addMapPoint(title: title, latitude: Double(latitude)!, longitude: Double(longitude)!, description: description)
            }
        }
    }
    
    func addMapPoint(title:String, latitude:Double, longitude:Double, description:String?)
    {
        let mapPoint = MKPointAnnotation();
        
        mapPoint.coordinate = CLLocationCoordinate2D(latitude: latitude,longitude:longitude)
        mapPoint.title      = title
        
        if description != nil
        {
            mapPoint.subtitle = description
        }
        
        mapView.addAnnotation(mapPoint)
    }
    
    func getMapPoints()->NSArray
    {
        var mapPoints: NSArray?
        
        if let path = Bundle.main.path(forResource: "MapPoints", ofType: "plist")
        {
            mapPoints = NSArray(contentsOfFile: path)
        }
        
        return mapPoints!
    }
    
    func kilometersfromPlace(from: CLLocationCoordinate2D, to:CLLocationCoordinate2D)->Double
    {
        let locA = CLLocation(latitude: from.latitude,longitude: from.longitude)
        let locB = CLLocation(latitude: to.latitude,longitude: to.longitude)
        
        let distance = locA.distance(from: locB) / 1000
    
        return distance
    }
    
    func showAllMapPoints()
    {
        mapView.removeAnnotations(mapView.annotations)
        let mapPoints = self.getMapPoints()
        self.addMapPoints(points: mapPoints as! Array<NSDictionary>)
    }
    
    func showOnlyNearbyMapPoints(fromLatitude: Double, fromLongitude: Double)
    {
        self.showAllMapPoints()
        
        let from = CLLocationCoordinate2D(latitude: fromLatitude,longitude:fromLongitude)
        self.addMapPoint(title: "Selected location", latitude: fromLatitude, longitude: fromLongitude, description: nil)
        
        for annotation in mapView.annotations
        {
            let distance = self.kilometersfromPlace(from: from, to: annotation.coordinate)
            
            if distance > 10
            {
                mapView.removeAnnotation(annotation)
            }
        }
    }
    
    func showOnlyWithDescriptionMapPoints()
    {
        mapView.removeAnnotations(mapView.annotations)
        let mapPoints = self.getMapPoints() as! Array<NSDictionary>
        
        for point in mapPoints
        {
            let title       = point["Title"] as! String
            let latitude    = point["Latitude"] as! String
            let longitude   = point["Longitude"] as! String
            let description   = point["Description"] as! String
            
            if !(title.isEmpty),!(latitude.isEmpty),!(longitude.isEmpty), !(description.isEmpty)
            {
                self.addMapPoint(title: title, latitude: Double(latitude)!, longitude: Double(longitude)!, description: description)
            }
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation])
    {
        self.myLocation = locations[0].coordinate
        let region = MKCoordinateRegion(center: locations[0].coordinate, span: mapView.region.span)
        
        mapView.setRegion(region, animated: true)
    }
    
    func addNewMapPoint(coordinates: CLLocationCoordinate2D)
    {
        let title = String(coordinates.latitude)+"'"+String(coordinates.longitude)
        self.addMapPoint(title: title, latitude: coordinates.latitude, longitude: coordinates.longitude, description: "")
    }

    func mapView(_ mapView: MKMapView, didSelect view: MKAnnotationView)
    {
        let request             = MKDirectionsRequest()
        request.source          = MKMapItem(placemark: MKPlacemark(coordinate: self.myLocation, addressDictionary: nil))
        request.destination     = MKMapItem(placemark: MKPlacemark(coordinate:(view.annotation?.coordinate)!, addressDictionary: nil))
        request.transportType   = .automobile
        
        let directions          = MKDirections(request:request)
        
        directions.calculate
        {response, error in
            if let route = response?.routes.first
            {
                self.mapView.add(route.polyline, level: MKOverlayLevel.aboveRoads)
            }
        }
    }
    
    func mapView(_ mapView: MKMapView, rendererFor overlay: MKOverlay) -> MKOverlayRenderer
    {
        let renderer = MKPolylineRenderer(overlay:overlay)
        renderer.lineWidth = 2
        renderer.strokeColor = #colorLiteral(red: 0.949, green: 0.1098, blue: 0, alpha: 1)
        
        return renderer
    }
}

