//
//  ThirdViewController.swift
//  Maps
//
//  Created by Teiksma  on 13.04.18.
//  Copyright © 2018. g. janisengers. All rights reserved.
//

import UIKit
import MapKit

class ThirdViewController: UIViewController, UITextFieldDelegate
{
    var delegate: ThirdViewControllerDelegate?
    
    @IBOutlet weak var latitudeTexBox: UITextField!
    @IBOutlet weak var longitudeTextBox: UITextField!
    
    @IBOutlet weak var textboxNoticeLabel: UILabel!
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        latitudeTexBox.delegate = self
        longitudeTextBox.delegate = self
    }
    
    @IBAction func addButtonAction(_ sender: Any)
    {
        
        if  let latidude     = Double(latitudeTexBox.text!),
            let longitude    = Double(longitudeTextBox.text!)
        {
            let loc = CLLocationCoordinate2D(latitude: latidude,longitude:longitude)
            
            if CLLocationCoordinate2DIsValid(loc)
            {
                self.textboxNoticeLabel.isHidden = true
                delegate?.addNewMapPoint(coordinates:loc)
            }
            else
            {
                self.textboxNoticeLabel.textColor = #colorLiteral(red: 0.949, green: 0.1098, blue: 0, alpha: 1)
                self.textboxNoticeLabel.text = "Invalid coordinates"
                self.textboxNoticeLabel.isHidden = false
            }
        }
        else
        {
            self.textboxNoticeLabel.textColor = #colorLiteral(red: 0.1947486302, green: 0.4189995233, blue: 1, alpha: 1)
            self.textboxNoticeLabel.text = "Enter latitude and longitude"
            self.textboxNoticeLabel.isHidden = false
        }
    }
    
    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool
    {
        if (textField.text == "" && string == "-") || (textField.text == "-" && string == "")
        {
            return true
        }
        
        if textField.text != "" || string != ""
        {
            let res = (textField.text ?? "") + string
            return Double(res) != nil
        }
        
        return true
    }
}

protocol ThirdViewControllerDelegate: class
{
    func addNewMapPoint(coordinates:CLLocationCoordinate2D)
}
